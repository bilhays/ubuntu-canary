Copyright 2010, 2011, 2016 by bil hays

To use these, all you should have to do is rsync a subdir to /
Mostly these are for ubuntu 16.04 LTS and should work on fairly recent versions. Some should work on redhat/centos with no or minor mods. Should is big word.

These are tailored for nagios/nrpe, but also run as standalones, so you could hook them to cron and email results with some kind of wrapper.

CANARY contains some software to read temperatures from TEMPer sensors. If you don't have any of these, I don't necessarily recommend them, you're probably better off with newer kit and a raspberry pi. I have two different types of TEMPer units, there are more. One uses pcsensor and the other uses temper.py The versions supplied here are slightly modified, all I need is the temperature in F for nagios to read. One thing to note, aside from the vendor code issues, at some point tempers started using the HID interface, so they are particular about what program can invoke them.

The beginning of this particular run into the woods began here:
https://relavak.wordpress.com/2009/10/17/temper-temperature-sensor-linux-driver/

Original source for the sensor code I stole and modified is here:
https://github.com/peterfarsinsen/pcsensor
My slightly modified source is provided.

This works on some older temper units.
http://www.manialabs.us/downloads/Temper.py

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

These programs are distributed in the hope that they will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


